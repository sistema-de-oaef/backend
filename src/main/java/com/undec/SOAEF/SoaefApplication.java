package com.undec.SOAEF;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoaefApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoaefApplication.class, args);
	}

}
