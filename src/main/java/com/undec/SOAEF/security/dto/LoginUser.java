package com.undec.SOAEF.security.dto;

import javax.validation.constraints.NotBlank;

public class LoginUser {
    @NotBlank
    private String userName;
    @NotBlank
    private String password;

    public @NotBlank String getUserName() {
        return userName;
    }

    public void setUserName(@NotBlank String userName) {
        this.userName = userName;
    }

    public @NotBlank String getPassword() {
        return password;
    }

    public void setPassword(@NotBlank String password) {
        this.password = password;
    }
}
