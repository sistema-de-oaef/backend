package com.undec.SOAEF.users.controller;
import com.undec.SOAEF.security.utiles.Mensaje;
import com.undec.SOAEF.users.dto.UpdateUserRequestModel;
import com.undec.SOAEF.users.dto.NewUserRequestModel;
import com.undec.SOAEF.users.entity.Role;
import com.undec.SOAEF.users.entity.User;
import com.undec.SOAEF.users.enums.RoleName;
import com.undec.SOAEF.users.service.RoleService;
import com.undec.SOAEF.users.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/usuario")
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("/lista")
    public ResponseEntity<List<User>> listUsers(){
        List<User> users = userService.listUser();
        return new ResponseEntity(users, HttpStatus.OK);
    }

    @GetMapping("/detalle/{id}")
    public ResponseEntity<User> getUser(@PathVariable("id") int id){
        if(!userService.existsById(id))
            return new ResponseEntity(new Mensaje("El usuario con id " + id + " no existe"), HttpStatus.NOT_FOUND);
        User user = userService.getUserById(id).get();
        return new ResponseEntity(user, HttpStatus.OK);
    }

    @PostMapping("/registrar")
    public ResponseEntity<?> registerUser(@Valid @RequestBody NewUserRequestModel newUserRequestModel, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(new Mensaje("Ingrese datos válidos!"), HttpStatus.BAD_REQUEST);
        }
        if (userService.existsByUserName(newUserRequestModel.getUserName())) {
            return new ResponseEntity<>(new Mensaje("El nombre de usuario ya existe!"), HttpStatus.BAD_REQUEST);
        }
        if (userService.existsByEmail(newUserRequestModel.getEmail())) {
            return new ResponseEntity<>(new Mensaje("El email ingresado se encuentra registrado en el sistema"), HttpStatus.BAD_REQUEST);
        }
        User user = new User(newUserRequestModel.getName(), newUserRequestModel.getLastName(),
                newUserRequestModel.getUserName(), newUserRequestModel.getEmail(),
                passwordEncoder.encode(newUserRequestModel.getPassword()));

        Set<Role> roles = new HashSet<>();
        if (newUserRequestModel.getRoles() == null || newUserRequestModel.getRoles().isEmpty()) {
            return new ResponseEntity<>(new Mensaje("Seleccione un rol válido!"), HttpStatus.BAD_REQUEST);
        } else {
            for (String role : newUserRequestModel.getRoles()) {
                switch (role.toLowerCase()) {
                    case "admin":
                        roles.add(roleService.findByNombre(RoleName.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role no encontrado.")));
                        roles.add(roleService.findByNombre(RoleName.ROLE_DIRECTOR_CARRERA)
                                .orElseThrow(() -> new RuntimeException("Error: Role no encontrado.")));
                        roles.add(roleService.findByNombre(RoleName.ROLE_BEDELIA)
                                .orElseThrow(() -> new RuntimeException("Error: Role no encontrado.")));
                        break;
                    case "director_carrera":
                        roles.add(roleService.findByNombre(RoleName.ROLE_DIRECTOR_CARRERA)
                                .orElseThrow(() -> new RuntimeException("Error: Role no encontrado.")));
                        break;
                    case "bedelia":
                        roles.add(roleService.findByNombre(RoleName.ROLE_BEDELIA)
                                .orElseThrow(() -> new RuntimeException("Error: Role no encontrado.")));
                        break;
                    default:
                        return new ResponseEntity<>(new Mensaje("Rol seleccionado no válido!"), HttpStatus.BAD_REQUEST);
                }
            }
        }
        user.setRoles(roles);
        userService.save(user);
        return new ResponseEntity<>("Usuario registrado correctamente", HttpStatus.OK);
    }


    @PutMapping("/actualizar/{id}")
    public ResponseEntity<?> actualizarUsuario(@PathVariable("id") Integer id, @Valid @RequestBody UpdateUserRequestModel updateUserRequestModel, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(new Mensaje("Ingrese datos válidos!"), HttpStatus.BAD_REQUEST);
        }

        if (!userService.existsById(id)) {
            return new ResponseEntity<>(new Mensaje("El usuario no existe"), HttpStatus.NOT_FOUND);
        }

        if (userService.existsByUserName(updateUserRequestModel.getUserName()) &&
                !userService.getUserById(id).get().getUserName().equals(updateUserRequestModel.getUserName())) {
            return new ResponseEntity<>(new Mensaje("El nombre de usuario ya existe"), HttpStatus.BAD_REQUEST);
        }

        if (userService.existsByEmail(updateUserRequestModel.getEmail()) &&
                !userService.getUserById(id).get().getEmail().equals(updateUserRequestModel.getEmail())) {
            return new ResponseEntity<>(new Mensaje("El email ya se encuentra registrado en el sistema"), HttpStatus.BAD_REQUEST);
        }

        User user = userService.getUserById(id).orElseThrow(() -> new RuntimeException("Error: Usuario no encontrado"));
        user.setName(updateUserRequestModel.getName());
        user.setLastName(updateUserRequestModel.getLastName());
        user.setUserName(updateUserRequestModel.getUserName());
        user.setEmail(updateUserRequestModel.getEmail());

        Set<Role> roles = new HashSet<>();
        if (updateUserRequestModel.getRoles() == null || updateUserRequestModel.getRoles().isEmpty()) {
            return new ResponseEntity<>(new Mensaje("Seleccione un rol válido!"), HttpStatus.BAD_REQUEST);
        } else {
            for (String role : updateUserRequestModel.getRoles()) {
                switch (role.toLowerCase()) {
                    case "admin":
                        roles.add(roleService.findByNombre(RoleName.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role no encontrado.")));
                        roles.add(roleService.findByNombre(RoleName.ROLE_DIRECTOR_CARRERA)
                                .orElseThrow(() -> new RuntimeException("Error: Role no encontrado.")));
                        roles.add(roleService.findByNombre(RoleName.ROLE_BEDELIA)
                                .orElseThrow(() -> new RuntimeException("Error: Role no encontrado.")));
                        break;
                    case "director_carrera":
                        roles.add(roleService.findByNombre(RoleName.ROLE_DIRECTOR_CARRERA)
                                .orElseThrow(() -> new RuntimeException("Error: Role no encontrado.")));
                        break;
                    case "bedelia":
                        roles.add(roleService.findByNombre(RoleName.ROLE_BEDELIA)
                                .orElseThrow(() -> new RuntimeException("Error: Role no encontrado.")));
                        break;
                    default:
                        return new ResponseEntity<>(new Mensaje("Rol seleccionado no válido!"), HttpStatus.BAD_REQUEST);
                }
            }
        }
        user.setRoles(roles);
        userService.save(user);
        return new ResponseEntity<>(new Mensaje("Usuario actualizado correctamente"), HttpStatus.OK);
    }

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<?> delete(@PathVariable("id")int id){
        if(!userService.existsById(id))
            return new ResponseEntity(new Mensaje("No existe el usuario"), HttpStatus.NOT_FOUND);
        userService.deleteUser(id);
        User user = userService.getUserById(id).get();
        return new ResponseEntity(new Mensaje("Se elimino el usuario " + user.getId()+ " : " + user.getName() + " " + user.getLastName()), HttpStatus.OK);
    }
}
