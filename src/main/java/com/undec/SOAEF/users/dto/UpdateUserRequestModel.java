package com.undec.SOAEF.users.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

public class UpdateUserRequestModel {
    @NotBlank
    private String name;
    @NotBlank
    private String lastName;
    @NotBlank
    private String userName;
    @Email
    private String email;
    private Set<String> roles = new HashSet<>();

    public UpdateUserRequestModel(){}

    public UpdateUserRequestModel(String name, String lastName, String userName, String email) {
        this.name = name;
        this.lastName = lastName;
        this.userName = userName;
        this.email = email;
    }

    public UpdateUserRequestModel(String name, String lastName, String userName, String email, Set<String> roles) {
        this.name = name;
        this.lastName = lastName;
        this.userName = userName;
        this.email = email;
        this.roles = roles;
    }

    public @NotBlank String getName() {
        return name;
    }

    public @NotBlank String getLastName() {
        return lastName;
    }

    public @NotBlank String getUserName() {
        return userName;
    }

    public @Email String getEmail() {
        return email;
    }

    public Set<String> getRoles() {
        return roles;
    }
}
