package com.undec.SOAEF.users.service;

import com.undec.SOAEF.users.entity.Role;
import com.undec.SOAEF.users.enums.RoleName;
import com.undec.SOAEF.users.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    public Optional<Role> findByNombre(RoleName roleName){
        return roleRepository.findByRoleName(roleName);
    }
}
